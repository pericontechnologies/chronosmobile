package com.pericon.chronos.core;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.pericon.chronos.callback.IBluetoothCallBack;

import java.util.List;

/**
 * Created by waqasnawaz on 2015-12-21.
 */
public class BluetoothDeviceSearch {

    private static BroadcastReceiver broadcastReceiver;
    private static boolean isTurnedOnByApp;

    public static void startDiscovery(Activity activity, IBluetoothCallBack callBack) {

        startDiscovery(activity, null, callBack);
    }

    public static void startDiscovery(Activity activity, final List<String> macAddressList, final IBluetoothCallBack callBack) {

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter!= null && !bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.enable();
            isTurnedOnByApp = true;
        }

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String action = intent.getAction();
                if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                    BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                    callBack.onDeviceFound(device);
                    if (macAddressList != null && macAddressList.contains(device.getAddress())) {
                        callBack.onDeviceMatched(device);
                    }
                }
            }
        };

        activity.registerReceiver(broadcastReceiver, filter);

        bluetoothAdapter.startDiscovery();
    }

    public static void stopDiscovery(Activity activity) {

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (isTurnedOnByApp && bluetoothAdapter!= null && bluetoothAdapter.isEnabled()) {
            bluetoothAdapter.disable();
            isTurnedOnByApp = false;
        }

        if (activity != null && broadcastReceiver != null) {
            activity.unregisterReceiver(broadcastReceiver);
        }
    }

}
