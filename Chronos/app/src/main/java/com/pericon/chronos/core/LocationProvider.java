package com.pericon.chronos.core;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.pericon.chronos.R;
import com.pericon.chronos.callback.ILocationCallBack;

/**
 * Created by waqasnawaz on 2015-12-21.
 */
public class LocationProvider implements LocationListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final long POLLING_FREQ = 5000;
    private static final long FASTEST_UPDATE_FREQ = 1000;
    private static final int MIN_SPEED_MOVING = 15;
    private static final int SPEED_STOPPED = 0;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;

    private ILocationCallBack callBack;
    private boolean notifyOnMovement;
    private boolean provideLocationUpdates;


    public LocationProvider(Context context, ILocationCallBack callBack) {

        this.callBack = callBack;

        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    public void requestLocationUpdates(boolean notifyOnMovement) {

        provideLocationUpdates = true;
        this.notifyOnMovement = notifyOnMovement;

        if(mLocationRequest == null) {
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(POLLING_FREQ);
            mLocationRequest.setFastestInterval(FASTEST_UPDATE_FREQ);
        }

        mGoogleApiClient.connect();
    }

    public void stopLocationUpdates() {

        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        provideLocationUpdates = false;
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (provideLocationUpdates) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

   @Override
    public void onLocationChanged(Location location) {

        if (notifyOnMovement) {
            double speed = convertToKMH(location.getSpeed());
            if (speed >= MIN_SPEED_MOVING){
                callBack.onMoving(location);
            }
            else if (speed == SPEED_STOPPED){
                callBack.onStopped(location);
            }
        }
        callBack.onLocationUpdated(location);
    }


    private double convertToKMH(double speed){

        return (speed * 18)/5;
    }

    private static void promptEnableGPS(final Activity activity) {

        Dialog dialog = new AlertDialog.Builder(activity)
                .setMessage(R.string.enable_gps_message)
                .setTitle(R.string.enable_gps_title)
                .setPositiveButton(R.string.install, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            activity.startActivity(intent);

                        } catch (Exception ex) {
                            Log.e(VoiceRecognition.class.getName(), ex.getMessage());
                        }
                    }
                })

                .setNegativeButton(R.string.cancel, null)
                .create();

        dialog.show();
    }

}
