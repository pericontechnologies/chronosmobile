package com.pericon.chronos.core;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by waqasnawaz on 2015-12-21.
 */
public class MapsFunctions {

    private static final double endLatitude = 43.8372079;
    private static final double endLongitude = -79.508276;
    private static final LatLng end = new LatLng(endLatitude, endLongitude);

    private static final double startLatitude = 43.653226;
    private static final double startLongitude = -79.38318;
    private static final LatLng start = new LatLng(startLatitude, startLongitude);

    public static void plotRoute(Activity activity, GoogleMap map){
        new MapsRouting(activity, map).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, start, end);
    }
}
