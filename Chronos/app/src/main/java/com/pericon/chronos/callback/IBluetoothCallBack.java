package com.pericon.chronos.callback;

import android.bluetooth.BluetoothDevice;

/**
 * Created by waqasnawaz on 2015-12-21.
 */
public interface IBluetoothCallBack {

    void onDeviceFound(BluetoothDevice device);
    void onDeviceMatched(BluetoothDevice device);
}
