package com.pericon.chronos.core;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.speech.RecognizerIntent;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.pericon.chronos.R;

import java.util.List;
import java.util.Locale;

/**
 * Created by waqasnawaz on 2015-12-21.
 */
public class VoiceRecognition {

    public static final int REQ_CODE_SPEECH_INPUT = 100;
    private static final String VOICE_RECOGNITION_APP_URI = "market://details?id=com.google.android.googlequicksearchbox";

    public static void promptSpeechInput(Activity activity) {

        promptSpeechInput(activity, null);
    }

    public static void promptSpeechInput(Activity activity, String prompt) {

        if (isSpeechRecognitionActivityPresent(activity)) {

            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

            if (prompt == null) {
                prompt = activity.getString(R.string.voice_prompt);
            }

            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, prompt);
            activity.startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);

        } else {
            installGoogleVoiceSearch(activity);
        }

    }

    private static boolean isSpeechRecognitionActivityPresent(Activity activity) {
        try {
            PackageManager pm = activity.getPackageManager();
            List activities = pm.queryIntentActivities(new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);

            if (activities.size() != 0) {
                return true;
            }
        } catch (Exception ex) {
            Log.e(VoiceRecognition.class.getName(), ex.getMessage());
        }
        return false;
    }

    private static void installGoogleVoiceSearch(final Activity activity) {

        Dialog dialog = new AlertDialog.Builder(activity)
                .setMessage(R.string.voice_app_not_installed)
                .setTitle(R.string.voice_app_install)
                .setPositiveButton(R.string.install, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(VOICE_RECOGNITION_APP_URI));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                            activity.startActivity(intent);

                        } catch (Exception ex) {
                            Log.e(VoiceRecognition.class.getName(), ex.getMessage());
                        }
                    }
                })

                .setNegativeButton(R.string.cancel, null)
                .create();

        dialog.show();
    }
}
