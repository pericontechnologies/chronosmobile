package com.pericon.chronos.model;

public interface Parser
{
    public Route parse();

    public Address parseAddress();
}