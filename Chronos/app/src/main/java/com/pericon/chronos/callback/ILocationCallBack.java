package com.pericon.chronos.callback;

import android.location.Location;

/**
 * Created by waqasnawaz on 2015-12-22.
 */
public interface ILocationCallBack {

    void onLocationUpdated(Location location);
    void onMoving(Location location);
    void onStopped(Location location);
}
