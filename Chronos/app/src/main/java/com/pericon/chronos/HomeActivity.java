package com.pericon.chronos;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.pericon.chronos.callback.IBluetoothCallBack;
import com.pericon.chronos.callback.ILocationCallBack;
import com.pericon.chronos.core.BluetoothDeviceSearch;
import com.pericon.chronos.core.LocationProvider;
import com.pericon.chronos.core.MapsFunctions;
import com.pericon.chronos.core.VoiceRecognition;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    LocationProvider locationProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Button testVoiceRecognitionBtn = (Button)findViewById(R.id.search_voice_btn);
        testVoiceRecognitionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VoiceRecognition.promptSpeechInput(HomeActivity.this);
            }
        });

        Button testBluetoothBtn = (Button)findViewById(R.id.bluetooth_btn);
        testBluetoothBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BluetoothDeviceSearch.startDiscovery(HomeActivity.this, new IBluetoothCallBack() {
                    @Override
                    public void onDeviceFound(BluetoothDevice device) {
                        Toast.makeText(HomeActivity.this, device.getName() + ", " + device.getAddress(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onDeviceMatched(BluetoothDevice device) {
                        Toast.makeText(HomeActivity.this, "Device Matched!", Toast.LENGTH_SHORT).show();
                        BluetoothDeviceSearch.stopDiscovery(HomeActivity.this);
                    }
                });
            }
        });

        /* Google route plot test*/

        ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMapAsync(new OnMapReadyCallback() {

            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.setMyLocationEnabled(true);
                //MapsFunctions.plotRoute(HomeActivity.this, googleMap);
            }
        });

        /* Location services/speed test*/

        locationProvider = new LocationProvider(this, new ILocationCallBack() {
            @Override
            public void onLocationUpdated(Location location) {
                Toast.makeText(HomeActivity.this, location.toString()+" speed: "+location.getSpeed(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onMoving(Location location) {
                Toast.makeText(HomeActivity.this, "Moving!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopped(Location location) {

            }
        });

        locationProvider.requestLocationUpdates(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case VoiceRecognition.REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    for (String result: results) {
                        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            }

        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camara) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        BluetoothDeviceSearch.stopDiscovery(HomeActivity.this);
        locationProvider.stopLocationUpdates();
    }
}
